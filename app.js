var Hapi = require('hapi');
var server = new Hapi.Server();
server.connection({
    port: 13000
});

var io = require('socket.io')(server.listener);
var users = {};

server.register(require('inert'), function(err) {
    'use strict';
    if (err) {
        throw err;
    }
    server.start(function() {
        console.log('Server running at:', server.info.uri);
        console.log('Server running on:', new Date());
    });
    server.route({
        method: 'GET',
        path: '/socket.io.js',
        handler: function(request, reply) {
            reply.file('./node_modules/socket.io-client/socket.io.js');
        }
    });
    server.route({
        method: 'GET',
        path: '/stylesheet.css',
        handler: function(request, reply) {
            reply.file('./css/foundation.css');
        }
    });
    server.route({
        method: 'GET',
        path: '/',
        handler: function(request, reply) {
            reply.file('index.html');
        }
    });
});


// var db = require('./db.js');
// var client = db.client;
io.on('connection', function(socket){
  var nick = 'No name';
  // io.emit('chat message', {msg: 'Żeby zmienić nick wpisz "/nick Janek"', name: 'Chat bot'});

  socket.on('chat message', function(msg){
    if (msg.charAt(0) === '/'){
      console.log('command', msg);
      switch (msg.split(" ", 1).join()) {
        case '/login':
          nick = msg.split(" ")[1];
          break;
        case '/nick':
          nick = msg.split(" ")[1];
          break;
        default:

      }
      if(msg.split(" ", 1).join() === '/login' || msg.split(" ", 1).join() === '/nick'){
        console.log(nick);
      }

    }
    else {
      io.emit('chat message', {msg: msg, name: nick});
      console.log('message: ' +nick+' '+msg);
    }
  });
});
